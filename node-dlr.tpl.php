<?php
// $Id: node.tpl.php,v 1.4.2.1 2009/05/12 18:41:54 johnalbin Exp $

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_user().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */

$preview_url = url($node->field_resource[0]['url'], array('query' => $node->field_resource[0]['query']));
$more_url = url($node->field_poductpage[0]['url'], array('query' => $node->field_poductpage[0]['query']));  // yes "poduct", that's a typo. it's on the field on the DB, so...
$km_tree = grepimporter_get_kompetansemaal($node);
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

    <h2 class="contentTitle"><?= $title ?></h2>

    <div class="mod banner">
        <div class="banner-content">
            <div class="mod-bd line">

                <div class="unit firstUnit size2of3">
                    <div class="colMargins">

                        <img src="/<?= check_url($node->field_big_thumbnail[0]["filepath"]) ?>" />

                    </div>
                </div>
                <div class="unit lastUnit size1of3">
                    <div class="colMargins">

                        <dl class="dataList">

                            <dt>Krever lisens</dt>
                            <dd><?= ($node->field_price[0]["view"] == "Gratis") ? "Nei" : "Ja" ?></dd>

                            <dt>Forlag</dt>
                            <dd><?= $node->field_forlag[0]["view"] ?></dd>

                            <dt>Språk</dt>
                            <dd><?= check_plain($node->field_language[0]["view"]) ?></dd>

                            <dt>Versjon</dt>
                            <dd><?= check_plain($node->field_version[0]["view"]) ?></dd>

                            <dt>Målgruppe</dt>
                            <dd><?= check_plain($node->field_enduserrole["view"]) ?></dd>

                            <dt>Årsplan inkl.</dt>
                            <dd><?= check_plain($node->field_yearplan[0]["view"]) ?></dd>

                            <dt>Nøkkelord</dt>
                            <dd><?= check_plain($node->field_keywords[0]["view"]) ?></dd>

                        </dl>

                        <div class="actions">
                            <ul>
                                <?php if ($more_url != "/") : ?>
                                    <li><a href="<?= $more_url ?>" class="action">Les mer om produktet</a></li>
                                <?php endif; ?>
                                <li><a href="<?= url('printmail/' . $node->nid) ?>" class="popMeUp-print-mail-form action">Tips en kollega</a></li>
                                <li>
                                    <a href="<?= $node_url ?>/last_ned" class="popMeUp-downloadBox action">Last ned LMS pakken</a>
                                    <a href="/hvordan_bruk_lms" class="actionHelp">Hva er LMS pakke?</a>
                                </li>
                                <?php if ($preview_url != "/") : ?>
                                    <li class="mainAction">
                                        <a href="<?= $preview_url ?>" class="action">
                                            <?= ($node->field_price[0]["view"] == "Gratis") ? "Gå til ressurs" : "Prøv ut ressurs" ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($node->field_price[0]["view"] != 'Gratis') : ?>
                                    <li><a href="<?= $more_url ?>" class="action">Kjøp lisens</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="line mainContent">
        <div class="unit firstUnit size2of3">
            <div class="colMargins">

                <div class="bodyText">
                    <?php print $node->content['body']['#value']; ?>
                </div>

                <?php if ($km_tree) : ?>

                    <h4>Støtter kompetansemålene</h4>

                    <div class="tabs">

                        <ul>

                            <?php $counter_ho = 0; ?>

                            <?php foreach ($km_tree as $hovedomraad => $kompetansemaalsetter) : ?>

                                <li><a href="#ho_<?= $counter_ho ?>"><?= htmlspecialchars($hovedomraad) ?></a></li>

                                <?php ++$counter_ho; ?>

                            <?php endforeach; ?>

                        </ul>

                        <?php $counter_ho = 0; ?>

                        <?php foreach ($km_tree as $hovedomraad => $kompetansemaalsetter) : ?>

                            <div id="ho_<?= $counter_ho; ?>" class="tabs">
                                <ul>

                                    <?php $counter_kms = 0; ?>

                                    <?php foreach ($kompetansemaalsetter as $kompetansemaalsett => $kompetansemaaler) : ?>

                                        <li><a href="#kms_<?= $counter_ho ?>_<?= $counter_kms ?>"><?= htmlspecialchars($kompetansemaalsett) ?></a></li>

                                        <?php ++$counter_kms; ?>

                                    <?php endforeach; ?>

                                </ul>

                                <?php $counter_kms = 0; ?>

                                <?php foreach ($kompetansemaalsetter as $kompetansemaalsett => $kompetansemaaler) : ?>

                                    <div id="kms_<?= $counter_ho ?>_<?= $counter_kms ?>" class="bodyText">

                                        <ul>

                                            <?php foreach ($kompetansemaaler as $kompetansemaal) : ?>

                                                <li><?= htmlspecialchars($kompetansemaal) ?></li>

                                            <?php endforeach;?>

                                        </ul>

                                    </div>

                                    <?php ++$counter_kms; ?>

                                <?php endforeach; ?>

                            </div>

                            <?php ++$counter_ho; ?>

                        <?php endforeach; ?>

                    </div> <!-- /tabs -->

                <?php endif; ?>

            </div>
        </div>
        <div class="unit lastUnit size1of3">
            <div class="colMargins">

                <div class="mod asideInfo">
                    <div class="asideInfo-content">

                        <h4 class="mod-hd">Kontaktinformasjon</h4>

                        <div class="mod-bd">

                            <?php if ($node->field_forlag[0]['thumb']) :?>
                                <img src="<?= $node->field_forlag[0]['thumb']; ?>" />
                            <?php endif; ?>

                            <?php if ($node->field_forlag[0]['contact']):?>
                                <?= $node->field_forlag[0]['contact']; ?><br />
                            <?php endif; ?>

                            <?php if ($node->field_forlag[0]['phone']):?>
                                Telefon: <?= $node->field_forlag[0]['phone']; ?><br />
                            <?php endif; ?>

                            <?php if ($node->field_forlag[0]['url']):?>
                                <?= $node->field_forlag[0]['url']; ?>
                            <?php endif; ?>

                        </div>

                    </div>
                </div>


                <?php if ($list = forlagene_get_aarstrinn($node)):?>
                <div class="mod asideInfo">
                    <div class="asideInfo-content">

                        <h4 class="mod-hd">Tilhører årstrinn</h4>

                        <div class="mod-bd">

                            <ul>
                            <?php foreach ($list as $link) {?>
                                <li><?= $link; ?></li>
                            <?php }?>
                            </ul>

                        </div>

                    </div>
                </div>
                <?php endif; ?>


                <?php if ($list = forlagene_get_fagomraader($node)):?>
                <div class="mod asideInfo">
                    <div class="asideInfo-content">

                        <h4 class="mod-hd">Tilhører fagområdene</h4>

                        <div class="mod-bd">

                            <ul>
                            <?php foreach ($list as $link) {?>
                                <li><?= $link; ?></li>
                            <?php }?>
                            </ul>

                        </div>

                    </div>
                </div>
                <?php endif; ?>


                <?php if ($list = forlagene_get_grep_relationlist($node, 'grep:laereplan')):?>
                <div class="mod asideInfo">
                    <div class="asideInfo-content">

                        <h4 class="mod-hd">Relatert til læreplanene</h4>

                        <div class="mod-bd">

                            <ul>
                            <?php foreach ($list as $link) {?>
                                <li><?= $link; ?></li>
                            <?php }?>
                            </ul>

                        </div>

                    </div>
                </div>
                <?php endif; ?>


            </div>
        </div>
    </div>

    <div class="mod metadata">
        <div class="metadata-content">
            <div class="mod-bd">
            <!--
                <?php if ($taxonomy): ?>
                    <div class="terms"><?php print $terms ?></div>
                <?php endif;?>

                <?php if ($links): ?>
                    <div class="links"><?php print $links; ?></div>
                <?php endif; ?>
                -->
            </div>
        </div>
    </div>

</div>
