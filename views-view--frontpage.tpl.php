<div class="line">
    <div class="unit size2of3 firstUnit">

        <div class="mod featured">
            <div class="featured-content ui-coverslide ui-coverslide-stack">
                <ul class="mod-bd slideshow">
                    <?= $rows ?>
                </ul>
            </div>
        </div>

    </div>
    <div class="unit size1of3 lastUnit">

        <div class="mod welcome">
            <div class="welcome-content">
                <h3 class="mod-hd">Velkommen!</h3>
                <p class="mod-bd"><?= check_plain(variable_get("site_mission", "")); ?></p>
            </div>
        </div>

    </div>
</div>