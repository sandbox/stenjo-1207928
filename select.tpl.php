<?php

$dropdowns_as_buttons = array(
    "sort_by",
    "sort_order",
    "items_per_page"
);

$dropdowns_as_checkboxes = array(
    "filter_aarstrinn",
    "filter_fagomraade",
    "filter_laereplaner",
);

?>

<?php if (in_array($element["#name"], $dropdowns_as_buttons)) : ?>

    <div class="mod <?= $element["#name"] ?>Widget">
        <div class="<?= $element["#name"] ?>Widget-content">
            <div class="mod-bd">

                <p class="label"><?= check_plain($element["#title"]) ?></p>

                <?php if (!empty($element["#value"])) : ?>

                    <input
                        type="hidden"
                        name="<?= check_plain($element["#name"]) ?>"
                        value="<?= check_plain($element["#value"]) ?>" />

                <?php endif; ?>

                <?php $firstClass = " first"; ?>

                <?php foreach ($element["#options"] as $value => $option) : ?>

                    <button
                        type="submit"
                        class="linkButton<?= $firstClass?><?= ($value == $element["#value"]) ? ' selected' : '' ?>"
                        name="<?= check_plain($element["#name"]) ?>"
                        value="<?= check_plain($value) ?>"><?= check_plain($option) ?></button>

                    <?php $firstClass = ""; ?>

                <?php endforeach; ?>

            </div>
        </div>
    </div>

<?php elseif (in_array($element["#name"], $dropdowns_as_checkboxes)) : ?>

    <?php $previous_depth = -1; ?>

    <?php foreach ($element["#options"] as $value => $option) : ?>

        <?php

            if (is_object($option)) {

                $value = key($option->option);
                $option = current($option->option);
            }

            $depth = strspn($option, "-");

            if ($depth === $previous_depth) {

                echo "</li>";
            }

            while ($depth > $previous_depth) {

                echo "<ol>";
                $depth--;
            }

            if ($depth < $previous_depth) {

                while ($depth < $previous_depth) {

                    echo "</li></ol>";
                    $depth++;
                }

                echo "</li>";
            }

            echo "<li>";

            $previous_depth = strspn($option, "-");

        ?>

        <label>
            <input
                    type="<?= $element["#multiple"] ? 'checkbox' : 'radio' ?>"
                    name="<?= check_plain($element["#name"]) . ($element["#multiple"] ? '[]' : '') ?>"
                    value="<?= check_plain($value) ?>"
                    <?= in_array($value, $element["#value"]) ? 'checked="checked"' : '' ?> />
            <?= check_plain(preg_replace('/^-+/', '', $option)) ?>
        </label>

    <?php endforeach; ?>

    <?php

    $depth = -1;

    while ($depth < $previous_depth) {

        echo "</li></ol>";
        $depth++;
    }

    ?>

<?php else : ?>

    <label><?= check_plain($element["#title"]) ?></label>

    <select name="<?= check_plain($element["#name"]) . ($element["#multiple"] ? '[]" multiple="multiple' : '') ?>">

    <?php foreach ($element["#options"] as $value => $option) : ?>

        <?php if (is_object($option)) : ?>

            <option
                <?= in_array(key($option->option), $element["#value"]) ? 'selected="selected"' : '' ?>
                value="<?= check_plain(key($option->option)) ?>"><?= check_plain(current($option->option)) ?></option>

        <?php else : ?>

            <option
                <?= $value == $element["#value"] ? 'selected="selected"' : '' ?>
                value="<?= check_plain($value) ?>"><?= check_plain($option) ?></option>

        <?php endif; ?>

    <?php endforeach; ?>

    </select>

<?php endif; ?>
