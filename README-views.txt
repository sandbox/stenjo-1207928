$view = new view;
$view->name = 'listing';
$view->description = 'Lists all DLRs (similar to /node)';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 3.0-alpha1;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['distinct'] = TRUE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Nullstill';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Sorter liste etter:';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '6';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
$handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Vis antall ressurser:';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '6, 12, 36';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'node';
$handler->display->display_options['row_options']['links'] = 1;
$handler->display->display_options['row_options']['comments'] = 0;
/* Relationship: Innhold: Forlag (field_forlag) */
$handler->display->display_options['relationships']['field_forlag_nid']['id'] = 'field_forlag_nid';
$handler->display->display_options['relationships']['field_forlag_nid']['table'] = 'node_data_field_forlag';
$handler->display->display_options['relationships']['field_forlag_nid']['field'] = 'field_forlag_nid';
$handler->display->display_options['relationships']['field_forlag_nid']['required'] = 0;
/* Field: Node: Teaser */
$handler->display->display_options['fields']['teaser']['id'] = 'teaser';
$handler->display->display_options['fields']['teaser']['table'] = 'node_revisions';
$handler->display->display_options['fields']['teaser']['field'] = 'teaser';
$handler->display->display_options['fields']['teaser']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['trim'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['teaser']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['teaser']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['html'] = 0;
$handler->display->display_options['fields']['teaser']['hide_empty'] = 0;
$handler->display->display_options['fields']['teaser']['empty_zero'] = 0;
/* Field: Node: Tittel */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 0;
/* Field: Taksonomi: Term */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = 0;
/* Sort criterion: Node: Tittel */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
$handler->display->display_options['sorts']['title']['exposed'] = TRUE;
$handler->display->display_options['sorts']['title']['expose']['label'] = 'A-Å';
/* Sort criterion: Node: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
$handler->display->display_options['sorts']['created']['exposed'] = TRUE;
$handler->display->display_options['sorts']['created']['expose']['label'] = 'Nyeste';
/* Sort criterion: Node: Tittel */
$handler->display->display_options['sorts']['title_1']['id'] = 'title_1';
$handler->display->display_options['sorts']['title_1']['table'] = 'node';
$handler->display->display_options['sorts']['title_1']['field'] = 'title';
$handler->display->display_options['sorts']['title_1']['relationship'] = 'field_forlag_nid';
$handler->display->display_options['sorts']['title_1']['exposed'] = TRUE;
$handler->display->display_options['sorts']['title_1']['expose']['label'] = 'Forlag';
/* Argument: Taksonomi: Term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '10';
$handler->display->display_options['arguments']['term_node_tid_depth']['break_phrase'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['set_breadcrumb'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['use_taxonomy_term_path'] = 0;
/* Filtrer: Node: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dlr' => 'dlr',
);
$handler->display->display_options['filters']['type']['expose']['operator'] = FALSE;
/* Filtrer: Node: Publisert */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth_1']['id'] = 'term_node_tid_depth_1';
$handler->display->display_options['filters']['term_node_tid_depth_1']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth_1']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['operator'] = 'term_node_tid_depth_1_op';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['label'] = 'Årstrinn';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['identifier'] = 'filter_aarstrinn';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['reduce'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth_1']['vid'] = '13';
$handler->display->display_options['filters']['term_node_tid_depth_1']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_1']['depth'] = '10';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Fagområde';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'filter_fagomraade';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['reduce'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth']['vid'] = '12';
$handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '10';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth_2']['id'] = 'term_node_tid_depth_2';
$handler->display->display_options['filters']['term_node_tid_depth_2']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth_2']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth_2']['value'] = array(
  '36380' => '36380',
  '62132' => '62132',
  '62133' => '62133',
  '62134' => '62134',
  '62135' => '62135',
  '62136' => '62136',
  '62137' => '62137',
  '62138' => '62138',
  '75794' => '75794',
  '62139' => '62139',
  '36379' => '36379',
  '62122' => '62122',
  '62123' => '62123',
  '62124' => '62124',
  '62125' => '62125',
  '62126' => '62126',
  '62127' => '62127',
  '62128' => '62128',
  '62129' => '62129',
  '62130' => '62130',
  '62131' => '62131',
  '75793' => '75793',
  '36381' => '36381',
  '62143' => '62143',
  '62140' => '62140',
  '62141' => '62141',
  '62142' => '62142',
  '36382' => '36382',
  '62150' => '62150',
  '62151' => '62151',
  '62152' => '62152',
  '62153' => '62153',
  '62154' => '62154',
  '62155' => '62155',
  '62156' => '62156',
  '62157' => '62157',
  '62158' => '62158',
  '62159' => '62159',
);
$handler->display->display_options['filters']['term_node_tid_depth_2']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['operator'] = 'term_node_tid_depth_2_op';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['label'] = 'Lærelaner';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['identifier'] = 'filter_laereplaner';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['reduce'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_2']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth_2']['vid'] = '14';
$handler->display->display_options['filters']['term_node_tid_depth_2']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_2']['depth'] = '10';

/* Display: Årstrinn */
$handler = $view->new_display('page', 'Årstrinn', 'page_aarstrinn');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Argument: Taksonomi: Term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'default';
$handler->display->display_options['arguments']['term_node_tid_depth']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['code'] = '$vid = variable_get(\'grepimporter_aarstrinn_taxonomy\', _grepimporter_get_suggested_vid(\'trinn\'));
return grepimporter_view_listing_default_argument($vid);';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '10';
$handler->display->display_options['arguments']['term_node_tid_depth']['break_phrase'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['set_breadcrumb'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['use_taxonomy_term_path'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filtrer: Node: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dlr' => 'dlr',
);
$handler->display->display_options['filters']['type']['expose']['operator'] = FALSE;
/* Filtrer: Node: Publisert */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Fagområde';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'filter_fagomraade';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['reduce'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth']['vid'] = '12';
$handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '10';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth_2']['id'] = 'term_node_tid_depth_2';
$handler->display->display_options['filters']['term_node_tid_depth_2']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth_2']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth_2']['value'] = array(
  '36380' => '36380',
  '62132' => '62132',
  '62133' => '62133',
  '62134' => '62134',
  '62135' => '62135',
  '62136' => '62136',
  '62137' => '62137',
  '62138' => '62138',
  '75794' => '75794',
  '62139' => '62139',
  '36379' => '36379',
  '62122' => '62122',
  '62123' => '62123',
  '62124' => '62124',
  '62125' => '62125',
  '62126' => '62126',
  '62127' => '62127',
  '62128' => '62128',
  '62129' => '62129',
  '62130' => '62130',
  '62131' => '62131',
  '75793' => '75793',
  '36381' => '36381',
  '62143' => '62143',
  '62140' => '62140',
  '62141' => '62141',
  '62142' => '62142',
  '36382' => '36382',
  '62150' => '62150',
  '62151' => '62151',
  '62152' => '62152',
  '62153' => '62153',
  '62154' => '62154',
  '62155' => '62155',
  '62156' => '62156',
  '62157' => '62157',
  '62158' => '62158',
  '62159' => '62159',
);
$handler->display->display_options['filters']['term_node_tid_depth_2']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['operator'] = 'term_node_tid_depth_2_op';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['label'] = 'Lærelaner';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['identifier'] = 'filter_laereplaner';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['reduce'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_2']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth_2']['vid'] = '14';
$handler->display->display_options['filters']['term_node_tid_depth_2']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_2']['depth'] = '10';
$handler->display->display_options['path'] = 'aarstrinn';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Årstrinn';
$handler->display->display_options['menu']['weight'] = '1';
$handler->display->display_options['menu']['name'] = 'primary-links';

/* Display: Fagområde */
$handler = $view->new_display('page', 'Fagområde', 'page_fagomraade');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Argument: Taksonomi: Term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'default';
$handler->display->display_options['arguments']['term_node_tid_depth']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['code'] = '$vid = variable_get(\'grepimporter_fagomraade_taxonomy\', _grepimporter_get_suggested_vid(\'fag\'));
return grepimporter_view_listing_default_argument($vid);';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '10';
$handler->display->display_options['arguments']['term_node_tid_depth']['break_phrase'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['set_breadcrumb'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['use_taxonomy_term_path'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filtrer: Node: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dlr' => 'dlr',
);
$handler->display->display_options['filters']['type']['expose']['operator'] = FALSE;
/* Filtrer: Node: Publisert */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth_1']['id'] = 'term_node_tid_depth_1';
$handler->display->display_options['filters']['term_node_tid_depth_1']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth_1']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['operator'] = 'term_node_tid_depth_1_op';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['label'] = 'Årstrinn';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['identifier'] = 'filter_aarstrinn';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['reduce'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth_1']['vid'] = '13';
$handler->display->display_options['filters']['term_node_tid_depth_1']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_1']['depth'] = '10';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth_2']['id'] = 'term_node_tid_depth_2';
$handler->display->display_options['filters']['term_node_tid_depth_2']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth_2']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth_2']['value'] = array(
  '36380' => '36380',
  '62132' => '62132',
  '62133' => '62133',
  '62134' => '62134',
  '62135' => '62135',
  '62136' => '62136',
  '62137' => '62137',
  '62138' => '62138',
  '75794' => '75794',
  '62139' => '62139',
  '36379' => '36379',
  '62122' => '62122',
  '62123' => '62123',
  '62124' => '62124',
  '62125' => '62125',
  '62126' => '62126',
  '62127' => '62127',
  '62128' => '62128',
  '62129' => '62129',
  '62130' => '62130',
  '62131' => '62131',
  '75793' => '75793',
  '36381' => '36381',
  '62143' => '62143',
  '62140' => '62140',
  '62141' => '62141',
  '62142' => '62142',
  '36382' => '36382',
  '62150' => '62150',
  '62151' => '62151',
  '62152' => '62152',
  '62153' => '62153',
  '62154' => '62154',
  '62155' => '62155',
  '62156' => '62156',
  '62157' => '62157',
  '62158' => '62158',
  '62159' => '62159',
);
$handler->display->display_options['filters']['term_node_tid_depth_2']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['operator'] = 'term_node_tid_depth_2_op';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['label'] = 'Lærelaner';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['identifier'] = 'filter_laereplaner';
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_2']['expose']['reduce'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_2']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth_2']['vid'] = '14';
$handler->display->display_options['filters']['term_node_tid_depth_2']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_2']['depth'] = '10';
$handler->display->display_options['path'] = 'fagomraade';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Fagområde';
$handler->display->display_options['menu']['weight'] = '2';
$handler->display->display_options['menu']['name'] = 'primary-links';

/* Display: Læreplaner */
$handler = $view->new_display('page', 'Læreplaner', 'page_laereplaner');
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Argument: Taksonomi: Term ID (with depth) */
$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_action'] = 'default';
$handler->display->display_options['arguments']['term_node_tid_depth']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'php';
$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_options']['code'] = '$vid = variable_get(\'grepimporter_laereplan_taxonomy\', _grepimporter_get_suggested_vid(\'plan\'));
return grepimporter_view_listing_default_argument($vid);';
$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '10';
$handler->display->display_options['arguments']['term_node_tid_depth']['break_phrase'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['set_breadcrumb'] = 0;
$handler->display->display_options['arguments']['term_node_tid_depth']['use_taxonomy_term_path'] = 0;
/* Argument: Taksonomi: Related term ID (with depth) */
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['id'] = 'rel_term_node_tid_depth';
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['field'] = 'rel_term_node_tid_depth';
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['style_plugin'] = 'default_summary';
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['depth'] = '10';
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['break_phrase'] = 0;
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['set_breadcrumb'] = 0;
$handler->display->display_options['arguments']['rel_term_node_tid_depth']['use_taxonomy_term_path'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filtrer: Node: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dlr' => 'dlr',
);
$handler->display->display_options['filters']['type']['expose']['operator'] = FALSE;
/* Filtrer: Node: Publisert */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth_1']['id'] = 'term_node_tid_depth_1';
$handler->display->display_options['filters']['term_node_tid_depth_1']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth_1']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth_1']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['operator'] = 'term_node_tid_depth_1_op';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['label'] = 'Årstrinn';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['identifier'] = 'filter_aarstrinn';
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['expose']['reduce'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth_1']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth_1']['vid'] = '13';
$handler->display->display_options['filters']['term_node_tid_depth_1']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth_1']['depth'] = '10';
/* Filtrer: Taksonomi: Term ID (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Fagområde';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['use_operator'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'filter_fagomraade';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['single'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['reduce'] = 0;
$handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth']['vid'] = '12';
$handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '10';
$handler->display->display_options['path'] = 'laereplaner';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Læreplaner';
$handler->display->display_options['menu']['weight'] = '3';
$handler->display->display_options['menu']['name'] = 'primary-links';





/* NOTE: the blocks defined above should be configured to appear in the "top navigation" area, with these
   visibility rules:

Fagområde block:

<?php
if (strpos(drupal_get_path_alias($_GET['q']), "fagomraade") === 0) return true;
if (arg(0) != "taxonomy") return false;
$term = taxonomy_get_term(arg(2));
return (variable_get("grepimporter_fagomraade_taxonomy", false) == $term->vid);
?>

Læreplaner block:

<?php
if (strpos(drupal_get_path_alias($_GET['q']), "laereplaner") === 0) return true;
if (arg(0) != "taxonomy") return false;
$term = taxonomy_get_term(arg(2));
return (variable_get("grepimporter_laereplan_taxonomy", false) == $term->vid);
?>

Årstrinn block:

<?php
if (strpos(drupal_get_path_alias($_GET['q']), "aarstrinn") === 0) return true;
if (arg(0) != "taxonomy") return false;
$term = taxonomy_get_term(arg(2));
return (variable_get("grepimporter_aarstrinn_taxonomy", false) == $term->vid);
?>

*/


// ---------------------------------------------------------------------------------------------------------------------


// VIEW: navigation blocks (displayed for top-level menu; requires taxonomy lineage module)

$view = new view;
$view->name = 'navigation';
$view->description = 'Top-level navigation (base view)';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'term_data';
$view->is_cacheable = FALSE;
$view->api_version = 3.0-alpha1;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['items_per_page'] = 0;
$handler->display->display_options['distinct'] = TRUE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['style_plugin'] = 'lineage_nested';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = 0;
/* Field: Taxonomy: Term */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = 1;
/* Field: Taxonomy: Hierarchy */
$handler->display->display_options['fields']['lineage']['id'] = 'lineage';
$handler->display->display_options['fields']['lineage']['table'] = 'term_lineage';
$handler->display->display_options['fields']['lineage']['field'] = 'lineage';
$handler->display->display_options['fields']['lineage']['exclude'] = TRUE;
$handler->display->display_options['fields']['lineage']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['lineage']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['lineage']['alter']['trim'] = 0;
$handler->display->display_options['fields']['lineage']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['lineage']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['lineage']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['lineage']['alter']['html'] = 0;
$handler->display->display_options['fields']['lineage']['hide_empty'] = 0;
$handler->display->display_options['fields']['lineage']['empty_zero'] = 0;
/* Field: Taxonomy: Depth */
$handler->display->display_options['fields']['depth']['id'] = 'depth';
$handler->display->display_options['fields']['depth']['table'] = 'term_lineage';
$handler->display->display_options['fields']['depth']['field'] = 'depth';
$handler->display->display_options['fields']['depth']['exclude'] = TRUE;
$handler->display->display_options['fields']['depth']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['depth']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['depth']['alter']['trim'] = 0;
$handler->display->display_options['fields']['depth']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['depth']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['depth']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['depth']['alter']['html'] = 0;
$handler->display->display_options['fields']['depth']['hide_empty'] = 0;
$handler->display->display_options['fields']['depth']['empty_zero'] = 0;
/* Field: Taxonomy: Term ID */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'term_data';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
$handler->display->display_options['fields']['tid']['exclude'] = TRUE;
$handler->display->display_options['fields']['tid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['tid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['tid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['tid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['tid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['tid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['tid']['alter']['html'] = 0;
$handler->display->display_options['fields']['tid']['hide_empty'] = 0;
$handler->display->display_options['fields']['tid']['empty_zero'] = 0;

/* Display: Year level (Årstrinn) */
$handler = $view->new_display('block', 'Year level (Årstrinn)', 'block_1');
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter: Taxonomy: Vocabulary */
$handler->display->display_options['filters']['vid']['id'] = 'vid';
$handler->display->display_options['filters']['vid']['table'] = 'term_data';
$handler->display->display_options['filters']['vid']['field'] = 'vid';
$handler->display->display_options['filters']['vid']['value'] = array(
  '2' => '2',
);
$handler->display->display_options['filters']['vid']['expose']['operator'] = FALSE;
$handler->display->display_options['block_description'] = 'Årstrinn Navigation';

/* Display: Subject area (Fagområde) */
$handler = $view->new_display('block', 'Subject area (Fagområde)', 'block_2');
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter: Taxonomy: Vocabulary */
$handler->display->display_options['filters']['vid']['id'] = 'vid';
$handler->display->display_options['filters']['vid']['table'] = 'term_data';
$handler->display->display_options['filters']['vid']['field'] = 'vid';
$handler->display->display_options['filters']['vid']['value'] = array(
  '4' => '4',
);
$handler->display->display_options['filters']['vid']['expose']['operator'] = FALSE;
$handler->display->display_options['block_description'] = 'Fagområde navigation';

/* Display: Teaching plans (Læreplaner) */
$handler = $view->new_display('block', 'Teaching plans (Læreplaner)', 'block_3');
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter: Taxonomy: Vocabulary */
$handler->display->display_options['filters']['vid']['id'] = 'vid';
$handler->display->display_options['filters']['vid']['table'] = 'term_data';
$handler->display->display_options['filters']['vid']['field'] = 'vid';
$handler->display->display_options['filters']['vid']['value'] = array(
  '6' => '6',
);
$handler->display->display_options['filters']['vid']['expose']['operator'] = FALSE;
$handler->display->display_options['block_description'] = 'Læreplaner navigation';


// ---------------------------------------------------------------------------------------------------------------------


// VIEW: frontpage carroussel

$view = new view;
$view->name = 'frontpage';
$view->description = 'Frontpage with featured items';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 3.0-alpha1;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '9';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Node: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 0;
/* Field: Content: Big thumbnail (field_big_thumbnail) */
$handler->display->display_options['fields']['field_big_thumbnail_fid']['id'] = 'field_big_thumbnail_fid';
$handler->display->display_options['fields']['field_big_thumbnail_fid']['table'] = 'node_data_field_big_thumbnail';
$handler->display->display_options['fields']['field_big_thumbnail_fid']['field'] = 'field_big_thumbnail_fid';
$handler->display->display_options['fields']['field_big_thumbnail_fid']['label'] = 'Thumb';
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_fid']['label_type'] = 'custom';
$handler->display->display_options['fields']['field_big_thumbnail_fid']['format'] = 'url_plain';
/* Field: Node: Teaser */
$handler->display->display_options['fields']['teaser']['id'] = 'teaser';
$handler->display->display_options['fields']['teaser']['table'] = 'node_revisions';
$handler->display->display_options['fields']['teaser']['field'] = 'teaser';
$handler->display->display_options['fields']['teaser']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['trim'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['teaser']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['teaser']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['html'] = 0;
$handler->display->display_options['fields']['teaser']['hide_empty'] = 0;
$handler->display->display_options['fields']['teaser']['empty_zero'] = 0;
/* Field: Content: Big thumbnail (field_big_thumbnail) - data */
$handler->display->display_options['fields']['field_big_thumbnail_data']['id'] = 'field_big_thumbnail_data';
$handler->display->display_options['fields']['field_big_thumbnail_data']['table'] = 'node_data_field_big_thumbnail';
$handler->display->display_options['fields']['field_big_thumbnail_data']['field'] = 'field_big_thumbnail_data';
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data']['data_key'] = 'title';
/* Field: Content: Big thumbnail (field_big_thumbnail) - data */
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['id'] = 'field_big_thumbnail_data_1';
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['table'] = 'node_data_field_big_thumbnail';
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['field'] = 'field_big_thumbnail_data';
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_big_thumbnail_data_1']['data_key'] = 'alt';
/* Field: Node: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['path']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['path']['alter']['trim'] = 0;
$handler->display->display_options['fields']['path']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['path']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['path']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['path']['alter']['html'] = 0;
$handler->display->display_options['fields']['path']['hide_empty'] = 0;
$handler->display->display_options['fields']['path']['empty_zero'] = 0;
$handler->display->display_options['fields']['path']['absolute'] = 0;
/* Sort criterion: Global: Random */
$handler->display->display_options['sorts']['random']['id'] = 'random';
$handler->display->display_options['sorts']['random']['table'] = 'views';
$handler->display->display_options['sorts']['random']['field'] = 'random';
/* Filter: Node: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dlr' => 'dlr',
);
$handler->display->display_options['filters']['type']['expose']['operator'] = FALSE;
/* Filter: Node: Promoted to front page */
$handler->display->display_options['filters']['promote']['id'] = 'promote';
$handler->display->display_options['filters']['promote']['table'] = 'node';
$handler->display->display_options['filters']['promote']['field'] = 'promote';
$handler->display->display_options['filters']['promote']['value'] = '1';
$handler->display->display_options['filters']['promote']['expose']['operator'] = FALSE;
/* Filter: Node: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->display->display_options['path'] = 'hjem';


// ---------------------------------------------------------------------------------------------------------------------


// VIEW: new items (displayed on front page + RSS feed)

$view = new view;
$view->name = 'news';
$view->description = 'New items';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 3.0-alpha1;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Defaults */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '20';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Content: Forlag (field_forlag) */
$handler->display->display_options['relationships']['field_forlag_nid']['id'] = 'field_forlag_nid';
$handler->display->display_options['relationships']['field_forlag_nid']['table'] = 'node_data_field_forlag';
$handler->display->display_options['relationships']['field_forlag_nid']['field'] = 'field_forlag_nid';
$handler->display->display_options['relationships']['field_forlag_nid']['required'] = 0;
/* Field: Node: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 0;
/* Field: Node: Teaser */
$handler->display->display_options['fields']['teaser']['id'] = 'teaser';
$handler->display->display_options['fields']['teaser']['table'] = 'node_revisions';
$handler->display->display_options['fields']['teaser']['field'] = 'teaser';
$handler->display->display_options['fields']['teaser']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['trim'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['teaser']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['teaser']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['teaser']['alter']['html'] = 0;
$handler->display->display_options['fields']['teaser']['hide_empty'] = 0;
$handler->display->display_options['fields']['teaser']['empty_zero'] = 0;
/* Field: Content: Small thumbnail (field_small_thumbnail) */
$handler->display->display_options['fields']['field_small_thumbnail_fid']['id'] = 'field_small_thumbnail_fid';
$handler->display->display_options['fields']['field_small_thumbnail_fid']['table'] = 'node_data_field_small_thumbnail';
$handler->display->display_options['fields']['field_small_thumbnail_fid']['field'] = 'field_small_thumbnail_fid';
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_fid']['label_type'] = 'none';
$handler->display->display_options['fields']['field_small_thumbnail_fid']['format'] = 'url_plain';
/* Field: Content: Small thumbnail (field_small_thumbnail) - data */
$handler->display->display_options['fields']['field_small_thumbnail_data']['id'] = 'field_small_thumbnail_data';
$handler->display->display_options['fields']['field_small_thumbnail_data']['table'] = 'node_data_field_small_thumbnail';
$handler->display->display_options['fields']['field_small_thumbnail_data']['field'] = 'field_small_thumbnail_data';
$handler->display->display_options['fields']['field_small_thumbnail_data']['label'] = '';
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data']['data_key'] = 'title';
/* Field: Content: Small thumbnail (field_small_thumbnail) - data */
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['id'] = 'field_small_thumbnail_data_1';
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['table'] = 'node_data_field_small_thumbnail';
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['field'] = 'field_small_thumbnail_data';
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['label'] = '';
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['link_to_node'] = 0;
$handler->display->display_options['fields']['field_small_thumbnail_data_1']['data_key'] = 'alt';
/* Field: Node: Path */
$handler->display->display_options['fields']['path']['id'] = 'path';
$handler->display->display_options['fields']['path']['table'] = 'node';
$handler->display->display_options['fields']['path']['field'] = 'path';
$handler->display->display_options['fields']['path']['label'] = '';
$handler->display->display_options['fields']['path']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['path']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['path']['alter']['trim'] = 0;
$handler->display->display_options['fields']['path']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['path']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['path']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['path']['alter']['html'] = 0;
$handler->display->display_options['fields']['path']['hide_empty'] = 0;
$handler->display->display_options['fields']['path']['empty_zero'] = 0;
$handler->display->display_options['fields']['path']['absolute'] = 0;
/* Field: Node: Title */
$handler->display->display_options['fields']['title_1']['id'] = 'title_1';
$handler->display->display_options['fields']['title_1']['table'] = 'node';
$handler->display->display_options['fields']['title_1']['field'] = 'title';
$handler->display->display_options['fields']['title_1']['relationship'] = 'field_forlag_nid';
$handler->display->display_options['fields']['title_1']['label'] = 'Forlag';
$handler->display->display_options['fields']['title_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['title_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['title_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['title_1']['link_to_node'] = 0;
/* Field: Node: Path */
$handler->display->display_options['fields']['path_1']['id'] = 'path_1';
$handler->display->display_options['fields']['path_1']['table'] = 'node';
$handler->display->display_options['fields']['path_1']['field'] = 'path';
$handler->display->display_options['fields']['path_1']['relationship'] = 'field_forlag_nid';
$handler->display->display_options['fields']['path_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['path_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['path_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['path_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['path_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['path_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['path_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['path_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['path_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['path_1']['absolute'] = 0;
/* Sort criterion: Node: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter: Node: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dlr' => 'dlr',
);
$handler->display->display_options['filters']['type']['expose']['operator'] = FALSE;
/* Filter: Node: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block_1');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = 3;

/* Display: RSS feed */
$handler = $view->new_display('feed', 'RSS feed', 'feed_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['style_plugin'] = 'rss';
$handler->display->display_options['style_options']['mission_description'] = 1;
$handler->display->display_options['row_plugin'] = 'node_rss';
$handler->display->display_options['row_options']['item_length'] = 'teaser';
$handler->display->display_options['path'] = 'feed';
$handler->display->display_options['displays'] = array(
  'block_1' => 'block_1',
  'default' => 0,
);
$handler->display->display_options['sitename_title'] = 1;
