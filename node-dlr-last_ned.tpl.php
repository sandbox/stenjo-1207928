<?php
// $Id: node.tpl.php,v 1.4.2.1 2009/05/12 18:41:54 johnalbin Exp $

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_user().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */

$more_url = url($node->field_poductpage[0]['url'], array('query' => $node->field_productpage[0]['query']));

?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

    <h2 class="contentTitle"><?= $title ?> — Last ned</h2>

    <div class="mod banner">
        <div class="banner-content">
            <div class="mod-bd line">

                <div class="unit firstUnit size2of3">
                    <div class="colMargins">

                        <img src="/<?= check_url($node->field_big_thumbnail[0]["filepath"]) ?>" />

                    </div>
                </div>
                <div class="unit lastUnit size1of3">
                    <div class="colMargins">

                        <dl class="dataList">

                            <dt>Pris</dt>
                            <dd><?= check_plain($node->field_price[0]["view"]) ?></dd>

                            <dt>Forlag</dt>
                            <dd><?= $node->field_forlag[0]["view"] ?></dd>

                            <dt>Språk</dt>
                            <dd><?= check_plain($node->language) ?></dd>

                            <dt>Versjon</dt>
                            <dd><?= check_plain($node->field_version[0]["view"]) ?></dd>

                            <dt>Målgruppe</dt>
                            <dd><?= check_plain($node->field_enduserrole[0]["view"]) ?></dd>

                            <dt>Årsplan inkl.</dt>
                            <dd><?= check_plain($node->field_yearplan[0]["view"]) ?></dd>

                            <dt>Nøkkelord</dt>
                            <dd><?= check_plain($node->field_keywords[0]["view"]) ?></dd>

                        </dl>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="downloadBox" class="mod downloadBox">
        <div class="downloadBox-content">
            <div class="mod-bd bodyText">

                <h3><a href="<?= url($node->field_download[0]['url']) ?>">Last ned <q><?= $title ?></q> nå</a></h3>
                <p>LMS-pakken består av et sett med lenker til den digitale læringsressursen. For å nyttiggjøre
                   deg disse lenkene, må pakken først legges inn i selve LMS'et.</p>

                <?php if ($node->field_price[0]["view"] != 'Gratis') : ?>

                    <p>OBS! For å få tilgang til selve den digitale læringsressursen, må brukeren ha lisens. Har
                       du allerede lisens, vil du komme til ressursen etter at et tilgangskontrollsystem har
                       avklart om du har lisens for produktet eller ikke. Har du ikke lisens, må du skaffe deg
                       en slik før du får tilgang til ressursen.</p>

                    <p><a href="<?= $more_url ?>">Bestill lisens på forlagets nettsider</a>.</p>

                <?php endif; ?>

                <p><a href="/hvordan_bruk_lms">Hvordan ta i bruk
                    <abbr title="Learning Management System" lang="en">LMS</abbr>-pakken</a>?</p>
            </div>
        </div>
    </div>


</div>
