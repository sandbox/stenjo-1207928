<?php

$url_base = url(NULL, array('absolute' => TRUE));
$url_theme = $url_base . path_to_theme();

$vid = variable_get('grepimporter_fagomraade_taxonomy', _grepimporter_get_suggested_vid('fag'));
$fagomraader = taxonomy_node_get_terms_by_vocabulary($node, $vid);

$forlag = node_load($node->field_forlag[0]['nid']);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="<?= $print['language']; ?>">
<head>
    <title><?= $print['title']; ?></title>
    <style type="text/css">

        /* YUI Reset */
        div.lmpMail {color:#000;background:#FFF;}div.lmpMail body,div.lmpMail div,div.lmpMail dl,div.lmpMail dt,div.lmpMail dd,div.lmpMail ul,div.lmpMail ol,div.lmpMail li,div.lmpMail h1,div.lmpMail h2,div.lmpMail h3,div.lmpMail h4,div.lmpMail h5,div.lmpMail h6,div.lmpMail pre,div.lmpMail code,div.lmpMail form,div.lmpMail fieldset,div.lmpMail legend,div.lmpMail input,div.lmpMail textarea,div.lmpMail p,div.lmpMail blockquote,div.lmpMail th,div.lmpMail td{margin:0;padding:0;}div.lmpMail table{border-collapse:collapse;border-spacing:0;}div.lmpMail fieldset,div.lmpMail img{border:0;}div.lmpMail address,div.lmpMail caption,div.lmpMail cite,div.lmpMail code,div.lmpMail dfn,div.lmpMail em,div.lmpMail strong,div.lmpMail th,div.lmpMail var{font-style:normal;font-weight:normal;}div.lmpMail li{list-style:none;}div.lmpMail caption,div.lmpMail th{text-align:left;}div.lmpMail h1,div.lmpMail h2,div.lmpMail h3,div.lmpMail h4,div.lmpMail h5,div.lmpMail h6{font-size:100%;font-weight:normal;}div.lmpMail q:before,div.lmpMail q:after{content:'';}div.lmpMail abbr,div.lmpMail acronym{border:0;font-variant:normal;}div.lmpMail sup{vertical-align:text-top;}div.lmpMail sub{vertical-align:text-bottom;}div.lmpMail input,div.lmpMail textarea,div.lmpMail select{font-family:inherit;font-size:inherit;font-weight:inherit;}div.lmpMail input,div.lmpMail textarea,div.lmpMail select{*font-size:100%;}div.lmpMail legend{color:#000;}

        /* YUI Fonts */
        div.lmpMail body,div.lmpMail{font:13px/1.231 arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}div.lmpMail select,div.lmpMail input,div.lmpMail button,div.lmpMail textarea{font:99% arial,helvetica,clean,sans-serif;}div.lmpMail table{font-size:inherit;font:100%;}div.lmpMail pre,div.lmpMail code,div.lmpMail kbd,div.lmpMail samp,div.lmpMail tt{font-family:monospace;*font-size:108%;line-height:100%;}

        div.lmpMail .lmpMailBody {
            background-color: #F2F2F2;
            background-image: url("<?= $url_theme ?>/images/bg_top.png");
            font-family: Arial, Helvetica, sans-serif;
        }

        div.lmpMail .lmpMailContent {
            background-color: #FFF;
            margin: 20px;
            max-width: 800px;
            padding: 20px;
        }

        div.lmpMail h4 {
            font-family: AUdimat, Impact, Arial, Helvetica, sans-serif;
            font-size: 197%;  /* 26px */
            font-weight: bold;
            margin: 0 0 1.5em 0;
        }

        div.lmpMail p {
            line-height: 1.462;
            margin-bottom: 1.5em;
        }

        div.lmpMail a {
            color: #255895;
            text-decoration: none;
        }

        div.lmpMail a:hover,
        div.lmpMail a:focus {
            color: #AC175C;
            outline: none;
            text-decoration: underline;
        }

        div.lmpMail a:active {
            color: #909C15;
            text-decoration: underline;
        }

    </style>
</head>
<body>
    <div class="lmpMail">
        <div class="lmpMailBody">
            <div class="lmpMailContent">

                <p>Hei. Jeg fant følgende læremiddel på <a href="<?= $url_base ?>">Læremiddelportalen</a>, og tenkte
                   at dette kunne være av interesse for deg.</p>

                <table>
                    <tr>
                        <td valign="top" width="60">
                            <img
                                width="50"
                                height="50"
                                src="<?= $url_base ?><?= check_url($node->field_small_thumbnail[0]["filepath"]) ?>"
                                title="<?= check_plain($node->field_small_thumbnail_data) ?>"
                                alt="<?= check_plain($node->field_small_thumbnail_data_1) ?>" />

                        </td>
                        <td valign="top">
                            <h4><a href="<?= $print['url'] ?>"><?= $print['title'] ?></a></h4>

                            <?= $node->content['body']['#value']; ?>

                            <p>
                                <?php foreach ($fagomraader as $fagomraade): ?>
                                    <a href="<?= $url_base ?>fagomraade/<?= check_plain($fagomraade->tid) ?>"><?= check_plain($fagomraade->name) ?></a>,
                                <?php endforeach; ?>

                                <a href="<?= $url_base ?>node/<?= $forlag->nid ?>"><?= check_plain($forlag->title) ?></a>
                            </p>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</body>
</html>
