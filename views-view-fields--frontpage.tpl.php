
<img
    src="<?= check_url($fields["field_big_thumbnail_fid"]->content) ?>"
    title="<?= check_plain($fields["field_big_thumbnail_data"]->content) ?>"
    alt="<?= check_plain($fields["field_big_thumbnail_data_1"]->content) ?>" />

<div class="image-caption">
    <h3><a href="<?= check_url($fields["path"]->content) ?>"><?= $fields["title"]->content ?></a></h3>
    <?= check_markup($fields["teaser"]->content) ?>
</div>
