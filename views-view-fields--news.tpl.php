<?php

$vid = variable_get('grepimporter_fagomraade_taxonomy', _grepimporter_get_suggested_vid('fag'));
$row->vid = $row->node_vid;
$fagomraader = taxonomy_node_get_terms_by_vocabulary($row, $vid);

?>
<div class="mod dlrItem">
    <div class="dlrItem-content">
        <div class="mod-hd">

            <img
                width="50"
                height="50"
                src="<?= check_url($fields["field_small_thumbnail_fid"]->content) ?>"
                title="<?= check_plain($fields["field_small_thumbnail_data"]->content) ?>"
                alt="<?= check_plain($fields["field_small_thumbnail_data_1"]->content) ?>" />

            <h4><a href="<?= check_url($fields["path"]->content) ?>"><?= check_plain($fields["title"]->content) ?></a></h4>
        </div>
        <div class="mod-bd"><?= check_markup($fields["teaser"]->content) ?></div>
        <ul class="mod-ft navList">

            <?php foreach ($fagomraader as $fagomraade): ?>

                <li><a href="/fagomraade/<?= check_plain($fagomraade->tid) ?>"><?= check_plain($fagomraade->name) ?></a>,</li>

            <?php endforeach; ?>

            <li><a href="<?= check_url($fields["path_1"]->content) ?>"><?= check_plain($fields["title_1"]->content) ?></a></li>
        </ul>
    </div>
</div>
