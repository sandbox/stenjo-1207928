/**
 * Based off: jQuery UI coverslide
 *
 * Copyright (c) 2009 AUTHORS.txt (http://jqueryui.com/about)
 * Dual licensed under the MIT (MIT-LICENSE.txt)
 * and GPL (GPL-LICENSE.txt) licenses.
 *
 *
 * Depends:
 *   ui.core.js
 */
(function($) {

$.widget("ui.coverslide", {

	_init: function() {

		var container = this.element.parent();
		if(!container.hasClass('ui-coverslide')){
			container = $('<div/>').addClass('ui-coverslide').insertBefore(vfcontainer);
		}

		this.data = {
			ul: this.element,
			ulBounds: this.element.bounds(),
			total: $('> li', this.element).length,
			container: container
		};

		this._size();
		this._active();
		this._assign(true);
	},

	destroy: function() {

		return this;
	},

	select: function(e, event){
		if(!e.jquery){ e = $(e); }
		event = event ? event : null;

		this._click(event, e);
	},

	_assign: function(init){
		init = init ? init : false;

		var self = this, activeIndex = self.data.active.index;

		$('> li', self.element).each(function(i, li){
			li = $(li);

			if(!li.hasClass('ui-coverslide-stack-active')){
				var pos = i < activeIndex ? (activeIndex - i) : (i - activeIndex), left = i < activeIndex;
				pos = 10 - (pos < 0 ? 0 : pos);

				var klass = 'ui-coverslide-stack-' + ((pos < 0 ? 0 : pos) * 10);
				li.removeClass().addClass(klass + ' ' + klass + (left ? '-l' : '-r'));
			}

			if(init){
				li.click(function(event){
					self._click(event, $(this));
				});
			}
		});
	},

	_click: function(event, target){

		var self = this,
			data = self.data,
			current = data.active.element;

		if(target.get(0) == current.get(0)){ return; }

		var	klass = '',
			activeIndex = this.data.active.index = self._index(target);

		$('> li', self.element).each(function(i, li){
			li = $(li);
			var pos = i < activeIndex ? (activeIndex - i) : (i - activeIndex), left = i < activeIndex;

			pos = 10 - (pos < 0 ? 0 : pos);
			pos *= 10;
			if(pos < 0){ pos = 0; }

			var baseKlass = 'ui-coverslide-stack-',
				klass = baseKlass + pos + ' ' + baseKlass + pos + (i < activeIndex ? '-l' : '-r');

			if(pos == 0){
				li.removeClass().addClass(klass).removeAttr('style');
				return;
			}

			var same = (target.get(0) == li.get(0)),
				bounds = same ? data.sizes['ui-coverslide-stack-active'] : data.sizes[klass],
				width = bounds.inner.width,
				height = bounds.inner.height,
				speed = self.options.speed,
				callback = null,
				css = {width: width, height: height, top: bounds.endTop, left: bounds.endLeft};

			target.css("visibility", "visible");

			if(same){
				callback = function(){
					$(this).removeClass().addClass('ui-coverslide-stack-active').removeAttr('style');
				}
			}
			else{
				callback = function(){
					$(this).removeClass().addClass(klass).removeAttr('style');
				}
			}

			li.css({zIndex: bounds.zIndex}).animate(css, speed, callback);

		});

		this.data.active.element = target;
		this._trigger("change", event, {active: this.data.active});
	},

	_index: function(li){
		return $('> li', this.element).index(li);
	},

	_active: function(){
		var active = $('> li.ui-coverslide-stack-active', this.element), index = 0;

		if(active.length){
			index = this._index(active);
		}
		else if (this.options.startAt) {
		    index = this.options.startAt;
		    active = $('> li:nth-child(' + index + ')', this.element).addClass('ui-coverslide-stack-active');
		    if(index > 0){ index--; }
		}
		else {
			index = Math.ceil(this.data.total / 2);
			active = $('> li:nth-child(' + index + ')', this.element).addClass('ui-coverslide-stack-active');
			if(index > 0){ index--; }
		}

		$.extend(this.data, {active: {element: active, index: index}});
	},

	_size: function(){

		var self = this, ul = $('<ul/>');

		self.data.sizes = {};

		for(var i = 0; i < 10; i++){
			var index = (i * 10), klass = 'ui-coverslide-stack-' + index;
			ul.append( $('<li/>').addClass(klass + ' ' + klass + '-l') );
			self.data.sizes[(i*10)] = 0;
		}

		for(var i = 0; i < 10; i++){
			var index = (i * 10), klass = 'ui-coverslide-stack-' + index;
			ul.append( $('<li/>').addClass(klass + ' ' + klass + '-r') );
			self.data.sizes[(i*10)] = 0;
		}

		ul.append( $('<li/>').addClass('ui-coverslide-stack-active') );

		var test = $('<div/>')
			.css({left: -10000, top: -10000, position: 'absolute'})
			.addClass('coverslide-stack')
			.append(ul)
			.appendTo(this.data.container);

		var length = ul.children().length;

		ul.children().each(function(i, li){
			li = $(li);
			var b = li.bounds();

			$.extend(b, {endLeft: li.css('left'), endTop: li.css('top'), zIndex: li.css('zIndex')});

			if(i == length){
				self.data.sizes['ui-coverslide-stack-active'] = b;
			}
			else{
				self.data.sizes[li.attr('class')] = b;
			}
		});

		test.remove();
	}
});

$.extend($.ui.coverslide, {
	defaults: {
		speed: 400,
		startAt: null /* set an index # to start with that slide (1-based list); default is half-way through the list */
	},
	getter: ""
});

})(jQuery);
