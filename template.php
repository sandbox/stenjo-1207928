<?php
// $Id: template.php,v 1.16.2.2 2009/08/10 11:32:54 goba Exp $

/**
 * Sets the body-tag class attribute.
 */
function phptemplate_body_class($logged_in) {

    if ($logged_in) {
        print ' class="loggedIn"';
    }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function phptemplate_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function phptemplate_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}

/**
 * Make sure we are registering the theme functions
 */
function forlagene_theme() {

  return array(
    'print_mail_form' => array(
      'arguments' => array('form' => NULL)),
    );
}

/**
 * Modify the Send page to a friend form so that the message
 * are is removed.
 *
 * @param $form
 */
function forlagene_print_mail_form($form) {

  // Probably not needed as the rendering of the txt_message
  // form removes it from the filan form
  $form['txt_message']['#type'] = 'hidden';
  $form['txt_message']['#value'] = '<br/>';

  // Drupal remembers what is rendered and not, so rendering the
  // txt_message form here will effectively remove it from the final submission form
  // Note that a default message must be added or the form submission will give an error message.
  drupal_render($form['txt_message']);

  // Also remove the reset and cancel buttons
  drupal_render($form['btn_clear']);
  drupal_render($form['btn_cancel']);

  return drupal_render($form);
}
/**
 * Return a themed set of links.
 *
 * @param $links
 *   A keyed array of links to be themed.
 * @param $attributes
 *   A keyed array of attributes
 * @return
 *   A string containing an unordered list of links.
 */
function forlagene_links($links, $attributes = array('class' => 'links')) {
  global $language;
  $output = '';

  $attributes["class"] .= " navList";

  if (count($links) > 0) {
    $output = '<div class="mainNav"><div class="mainNav-content"><ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language->language)) {
        $class .= ' active';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul></div></div>';
  }

  return $output;
}

function forlagene_preprocess_node(&$vars) {

    // Switch to a different node-<something> template based on node properties.

    if ($vars['page']) {

    	$vars['template_files'] = array(
    	   'node-'. $vars['node']->type,
           'node-'. $vars['node']->type . '-' . arg(2),
    	   'node-'. $vars['node']->nid,
    	);
    }
    else {

        $vars['template_files'] = array(
            'node-page',
            'node-'. $vars['node']->type .'-page',
            'node-'. $vars['node']->nid .'-page',
        );
    }

    // Define node type-specific variables by calling their own preprocess functions (if they exist)

	$function = 'forlagene_preprocess_node'.'_'. $vars['node']->type;

	if (function_exists($function)) {

		$function(&$vars);
	}
}

function forlagene_preprocess_node_dlr(&$vars) {

	//drupal_set_message('<pre>'.print_r($vars['node']->field_poductpage, true).'</pre>');

	// Load the forlag node
	$fnid = $vars['node']->field_forlag[0]['nid'];
	$forlag = node_load($fnid);
	if (is_object($forlag)) {
		if (isset($forlag->field_small_thumbnail[0]['filepath'])) {
			$vars['node']->field_forlag[0]['thumb'] = url($forlag->field_small_thumbnail[0]['filepath']);
		}

		if (isset($forlag->field_link[0]['url'])) {
			$vars['node']->field_forlag[0]['url'] = l('Besøk våre nettsider', $forlag->field_link[0]['url'], array('absolute' => true));
		}

		if (isset($forlag->field_contact[0]['url'])) {
			$vars['node']->field_forlag[0]['contact'] = l('Kontakt oss', $forlag->field_contact[0]['url'], array('absolute' => true));
		}

		if (isset($forlag->field_phone[0]['value'])) {
			$vars['node']->field_forlag[0]['phone'] = check_plain($forlag->field_phone[0]['value']);
		}
		//		drupal_set_message('<pre>'.print_r($vars['node']->taxonomy, true).'</pre>');
	}

	// End user role handling
	$endusers = array();
	foreach ($vars['node']->field_enduserrole as $role) {
		$endusers[] = $role['view'];
	}
	$vars['node']->field_enduserrole['view'] = implode(', ', $endusers);

	// TODO: Add the price handling

}

/**
 * Themer function to retrieve the list of related taxonomy GREP terms
 *
 * @param Object $node
 * @param String $type
 *
 * @return Array list of links to taxonomy lists
 */
function forlagene_get_grep_relationlist($node, $type ) {

	$vid = variable_get('grepimporter_laereplan_taxonomy', 0);

	if ($vid) {

		// Get all the terms this node is linked to
		$items = db_query("	SELECT term_data.name, term_data.tid FROM {term_data}
							LEFT JOIN {term_node} ON (term_data.tid = term_node.tid)
							WHERE term_data.vid = '%d' AND term_node.nid = '%d'
							ORDER BY term_data.weight", $vid, $node->nid);
		$terms = array();
		while ($item = db_fetch_object($items)) {
			$terms[$item->tid] = $item->name;
			// Get the parents
			$parents = taxonomy_get_parents_all($item->tid);
			foreach ($parents as $ptid => $pname) {
				$terms[$pname->tid] = $pname->name;
			}
		}

		// Get only the læreplan level.
		$keys = array_keys($terms); // Get the terms

		if ($keys) {

    		$placeholders = implode(', ', array_fill(0, count($keys), "%d"));
    		$keys[] = $type; // Add final variable
    		$items = db_query("
                SELECT
                    tid,
                    type
                FROM
                    {grepimporter_rdf_uuid_tid_mapping}
                WHERE
                    tid IN ($placeholders) AND
                    type = '%s'
                ", $keys);

    		$result = array();

    		while($item = db_fetch_object($items)) {
    			$result[] = l($terms[$item->tid], 'laereplaner/' . $item->tid . '/all');
    		}

    		return $result;
		}
	}

	return array(t('No Læreplaner taxonomy found'));
}


function forlagene_get_fagomraader($node) {

	$vid = variable_get('grepimporter_fagomraade_taxonomy', 0);

	if ($vid) {
		$items = db_query("	SELECT term_data.name, term_data.tid FROM {term_data}
							LEFT JOIN {term_node} ON (term_data.tid = term_node.tid)
							WHERE term_data.vid = '%d' AND term_node.nid = '%d'
							ORDER BY term_data.weight", $vid, $node->nid);
		$result = array();
		while($item = db_fetch_object($items)) {
			$result[] = l($item->name, 'fagomraade/' . $item->tid . '/all');
		}
		return $result;
	}

	return array(t('No Fagområder taxonomy found'));

}

function forlagene_get_aarstrinn($node) {

	$vid = variable_get('grepimporter_aarstrinn_taxonomy', 0);

	if ($vid) {
		$items = db_query("	SELECT term_data.name, term_data.tid FROM {term_data}
							LEFT JOIN {term_node} ON (term_data.tid = term_node.tid)
							WHERE term_data.vid = '%d' AND term_node.nid = '%d'
							ORDER BY term_data.weight", $vid, $node->nid);
		$result = array();
		while($item = db_fetch_object($items)) {
			$result[] = l($item->name, 'aarstrinn/' . $item->tid . '/all');
		}
		return $result;
	}

	return array(t('No Årstrinn taxonomy found'));
}

/**
 * Retrieve the first (there should be only one configured) faceted search "environment"
 */
function forlagene_get_faceted_search_env() {

    $faceted_search_envs = array_values(faceted_search_get_env_ids());
    return faceted_search_env_load($faceted_search_envs[0]);
}

/**
 * Returns the rendered faceted search form
 */
function forlagene_get_faceted_search_form() {

    if (function_exists("faceted_search_ui_keyword_block")) {

        return faceted_search_ui_keyword_block(forlagene_get_faceted_search_env());
    }
    else {

        return "";
    }
}

function forlagene_faceted_search_ui_stage_results($content) {

    $output = '<div class="view-listing">' . $content . '</div>';
    return $output;
}

/**
 * Implementation of theme_faceted_search_ui_search_page
 */
function forlagene_faceted_search_ui_search_page($results, $type) {

    global $pager_total_items, $pager_page_array;

    $faceted_search_style = faceted_search_ui_get_style(forlagene_get_faceted_search_env());

    $items_per_page = $faceted_search_style->get_limit();
    $total_items = $pager_total_items[0];
    $current_page = $pager_page_array[0];

    $start = 1 + ($current_page * $items_per_page);
    $end = (1 + $current_page) * $items_per_page;

    if ($end > $total_items) $end = $total_items;

    $output  = '<div class="view-listing">';
    $output .= '<div class="view-listing-top">';

    if ($total_items == 1) {

        $output .= '<h3>1 ressurs</h3>';
    }
    elseif ($total_items && ($start == 1) && ($end == $total_items)) {

        $output .= '<h3>' . $total_items . ' ressurser — viser alle</h3>';
    }
    elseif ($total_items) {

        $output .= '<h3>' . $total_items . ' ressurser — viser ' . $start . '–' . $end . '</h3>';
    }
    else {

        $output .= '<h3>Ingen resultater funnet</h3>';
    }

    $output .= theme('pager', NULL, $items_per_page, 0);
    $output .= '</div>';

    foreach ($results as $entry) {
        $output .= theme('faceted_search_ui_search_item', $entry, $type);
    }

    $output .= '</div><div class="view-listing-bottom">';
    $output .= '
        <div class="views-exposed-widget">
            <div class="mod items_per_pageWidget">
                <div class="items_per_pageWidget-content">
                    <div class="mod-bd">
                        <p class="label">Vis antall ressurser:</p>
                        <a href="?items_per_page=6" class="first linkButton' . ($items_per_page == 6 ? ' selected' : '') . '">6</a>
                        <a href="?items_per_page=12" class="linkButton' . ($items_per_page == 12 ? ' selected' : '') . '">12</a>
                        <a href="?items_per_page=36" class="linkButton' . ($items_per_page == 36 ? ' selected' : '') . '">35</a>
                    </div>
                </div>
            </div>
        </div>';

    $output .= theme('pager', NULL, $items_per_page, 0);
    $output .= '</div>';

    return $output;
}
/**
 * Implementation of theme_faceted_search_ui_search_item
 */
function forlagene_faceted_search_ui_search_item($item, $type) {

    $node = $item['node'];

    $vid = variable_get('grepimporter_fagomraade_taxonomy', _grepimporter_get_suggested_vid('fag'));
    $fagomraader = taxonomy_node_get_terms_by_vocabulary($node, $vid);
    $fagomraader_markup = "";

    foreach ($fagomraader as $fagomraade) {

        $fagomraader_markup .= '<li><a href="/fagomraade/' . $fagomraade->tid . '">';
        $fagomraader_markup .= check_plain($fagomraade->name) . '</a>,</li>';
    }

    $forlag = node_load($node->field_forlag[0]['nid']);

    return '
        <div class="views-row">
            <div class="mod dlrItem">
                <div class="dlrItem-content">
                    <div class="mod-hd">
                        <img
                            width="50"
                            height="50"
                            src="/' . check_url($node->field_small_thumbnail[0]["filepath"]) . '"
                            title="' . check_plain($node->field_small_thumbnail[0]["data"]["title"]) . '"
                            alt="' . check_plain($node->field_small_thumbnail[0]["data"]["alt"]) . '" />

                        <h4><a href="' . check_url($item['link']) . '">' . check_plain($item['title']) . '</a></h4>
                    </div>
                    <div class="mod-bd">' . $node->teaser . '</div>
                    <ul class="mod-ft navList">
                        ' . $fagomraader_markup . '
                        <li><a href="/node/' . $forlag->nid . '">' . check_plain($forlag->title) . '</a></li>
                    </ul>
                </div>
            </div>
        </div>
    ';
}

/**
 * Implementation of theme_faceted_search_ui_facet_wrapper
 */
function forlagene_faceted_search_ui_facet_wrapper($faceted_search, $facet, $context, $content) {

    if ($context != "current") return $content;

    return $content . '<span class="h3">/</span> ';
}

/**
 * Implementation of theme_faceted_search_ui_facet_heading
 */
function forlagene_faceted_search_ui_facet_heading($env, $ui_state, $facets, $index, $context, $show_label = TRUE) {

    $output = '';

    if ($context == 'current') {

        $breadcrumb = faceted_search_ui_build_breadcrumb($env, $ui_state, $facets, $index, $breadcrumb, $context);
        $output .= theme('faceted_search_ui_breadcrumb', $breadcrumb);

        $remover_path = faceted_search_ui_build_remover_path($env, $ui_state, $facets, $index);
        $output .= theme('faceted_search_ui_remover_link_current_search', $remover_path) . ' ';
    }

    return $output;
}

/**
 * Implementation of theme_faceted_search_ui_remover_link_current_search
 */
function forlagene_faceted_search_ui_remover_link_current_search($path) {

    $options = array(
        'html' => TRUE,
        'attributes' => array('title' => 'Fjern dette filteret', 'class' => 'remove'),
    );

    return l('&times;', $path, $options);
}

/**
 * Implementation of theme_faceted_search_ui_guided_search
 */
function forlagene_faceted_search_ui_guided_search($search, $facets) {

    $output = '';
    $firstClass = ' first';

    foreach ($facets as $facet) {

        if ($facet) {

            $output .= '
                <div class="mod facetBlock' . $firstClass . '">
                    <div class="facetBlock-content">
                        ' . $facet . '
                    </div>
                </div>';

            $firstClass = '';
        }
    }

    return $output;
}

/**
 * Implementation of theme_faceted_search_ui_categories
 */
function forlagene_faceted_search_ui_categories($facet, $categories, $stage) {

    $title = ($facet instanceof taxonomy_facet) ? $facet->_vocabulary->name : $facet->get_label();

    if (count($categories)) {

        return '<p>Begrens til ' . $title . ' </p>' . theme('item_list', $categories);
    }
}

/**
 * Implementation of theme_faceted_search_ui_breadcrumb
 */
function forlagene_faceted_search_ui_breadcrumb($breadcrumb) {

    return '<span class="h3 currentlySelected">' . implode(' - ', $breadcrumb) . '</span>';
}

/**
 * Implementation of theme_faceted_search_ui_more
 */
function forlagene_faceted_search_ui_more($path) {
    if ($path) {
        return l('Mer…', $path, array('class' => 'faceted-search-more'));
    }
    else {
        return '<span class="faceted-search-more">Mer…</span>';
    }
}

/**
 * Default theme function for all RSS rows.
 */
function forlagene_preprocess_views_view_row_rss(&$vars) {

    $url_base = url(NULL, array('absolute' => TRUE));

    $view = &$vars['view'];
    $options = &$vars['options'];
    $item = &$vars['row'];

    // Use the [id] of the returned results to determine the nid in [results]
    $result = &$vars['view']->result;
    $id = &$vars['id'];
    $node = node_load($result[$id-1]->nid);

    if ($node->field_small_thumbnail[0]) {

        $image_url = $url_base . $node->field_small_thumbnail[0]['filepath'];

    } else {

        $image_url = $url_base . '/sites/default/files/imagefield_default_images/small_thumb.png';
    }

    $image = '<img align="left" hspace="10" vspace="10" src="' . htmlentities($image_url) . '" />';


    $vars['title'] = check_plain($item->title);
    $vars['link'] = check_url($item->link);
    $vars['description'] = '<![CDATA[' . $image . check_markup($node->teaser) . ']]>';
    $vars['node'] = $node;
    $vars['item_elements'] = empty($item->elements) ? '' : format_xml_elements($item->elements);
}