<?php
// $Id: views-exposed-form.tpl.php,v 1.4.2.3 2010/03/10 19:57:13 merlinofchaos Exp $
/**
 * @file views-exposed-form.tpl.php
 *
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php endif; ?>
<div class="views-exposed-form">
  <ul class="views-exposed-widgets clear-block">

    <?php if (!empty($widgets)): ?>

        <!-- BEGIN EXPOSED CONTROLS --><?php /* This comment is used in view-view--listing.tpl.php to filter content */ ?>
        <li class="views-exposed-widget exposed-widget-filter">

            <p class="label">Begrens søk etter:</p>

            <ul class="controlList">

                <?php foreach($widgets as $id => $widget): ?>
                  <li class="control">

                    <?php if (!empty($widget->label)): ?>
                        <p class="filterTitle"><?= $widget->label; ?></p>
                    <?php endif; ?>

                    <div class="mod filterBy">
                        <div class="filterBy-content">

                            <div class="mod-bd">
                                <?php if (!empty($widget->operator)): ?>
                                  <div class="views-operator">
                                    <?php print $widget->operator; ?>
                                  </div>
                                <?php endif; ?>
                                <div class="views-widget">
                                  <?php print $widget->widget; ?>
                                </div>
                            </div>

                            <div class="mod-ft">
                                <button type="submit" class="submitButton">Begrens</button>
                            </div>

                        </div>

                    </div>

                  </li>
                <?php endforeach; ?>

            </ul>

        </li>
        <!-- END EXPOSED CONTROLS --><?php /* This comment is used in view-view--listing.tpl.php to filter content */ ?>

    <?php endif; ?>

    <?php if (!empty($sort_by)): ?>
      <li class="views-exposed-widget">
        <?php print $sort_by; ?>
      </li>
    <?php endif; ?>

    <?php if (!empty($items_per_page)): ?>
      <li class="views-exposed-widget">
        <?php print $items_per_page; ?>
      </li>
    <?php endif; ?>
    <?php if (!empty($offset)): ?>
      <li class="views-exposed-widget">
        <?php print $offset; ?>
      </li>
    <?php endif; ?>
    <?php if (!empty($reset_button)): ?>
      <li class="views-exposed-widget">
        <?php print $reset_button; ?>
      </li>
    <?php endif; ?>

  </ul>
</div>
