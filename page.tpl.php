<?php
/**
 * @file page.tpl.php
 *
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the current
 *   path, whether the user is logged in, and so on.
 * - $body_classes_array: An array of the body classes. This is easier to
 *   manipulate then the string in $body_classes.
 * - $node: Full node object. Contains data that may not be safe. This is only
 *   available if the current page is on the node's primary url.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing primary navigation links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links for
 *   the site, if they have been configured.
 *
 * Page content (in order of occurrance in the default page.tpl.php):
 * - $left: The HTML for the left sidebar.
 *
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the view
 *   and edit tabs when displaying a node).
 *
 * - $content: The main content of the current Drupal page.
 *
 * - $right: The HTML for the right sidebar.
 *
 * Footer/closing data:
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer_message: The footer message as defined in the admin settings.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html
    xmlns="http://www.w3.org/1999/xhtml"
    xml:lang="<?php print $language->language ?>"
    lang="<?php print $language->language ?>"
    dir="<?php print $language->dir ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php print $head_title ?></title>

    <?php $theme_path = base_path() . path_to_theme(); ?>

    <link rel="stylesheet" type="text/css" href="<?= $theme_path ?>/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?= $theme_path ?>/css/jquery-ui.css" />
    <link rel="shortcut icon" href="<?= $theme_path ?>/images/favicon.png" />
    <link rel="alternate" type="application/rss+xml" title="Læremiddelportalen RSS" href="/feed" />

    <!--[if IE]><link type="text/css" rel="stylesheet" media="all" href="<?= $theme_path ?>/css/ie.css" /><![endif]-->
</head>

<body<?php print phptemplate_body_class($logged_in); ?>>

<script type="text/javascript">document.body.className += " js";</script>

<div class="page liquid">

    <div class="layoutTop">

        <div class="head headLMP">

            <div class="logo">
                <div class="logo-content">
                    <h1>
                        <?php $site_title = check_plain($site_name); ?>

                        <a href="<?= check_url($front_page) ?>" title="<?= $site_title ?>">
                            <img src="<?= $theme_path ?>/images/logo.png" width="405" height="60" alt="<?= $site_title ?>" />
                        </a>
                    </h1>
                </div>
            </div>

            <?php if (isset($primary_links)) : ?>
                <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
            <?php endif; ?>

            <div class="searchBox">
                <div class="searchBox-content">
                    <?php print forlagene_get_faceted_search_form(); ?>
                </div>
            </div>

            <div class="navigation">
                <?php print $navigation ?>
            </div><!-- /navigation -->

        </div><!-- /head -->

    </div><!-- /layoutTop -->

    <div class="layoutCentre">

        <div class="body bodyLMP">

            <div class="main">

                <?php if ($messages): print $messages; endif; ?>

                <?php if (arg(0) == "taxonomy"): print '<div class="listing">'; endif; ?>

                <?php print $content ?>

                <?php if (arg(0) == "taxonomy"): print '</div>'; endif; ?>

            </div><!-- /main -->

        </div><!-- /body -->

    </div><!-- /layoutCentre -->

    <div class="layoutBottom">

        <div class="foot footLMP">

            <ul class="navList courtesyNav">
                <li><a href="/om_portalen">Om portalen</a></li>
                <li><a href="/hvordan_bruk_lms">Hvordan ta i bruk LMS pakken?</a></li>
                <li><a href="/kontakt" class="popMeUp-contact-mail-page">Kontakt oss</a></li>
                <li><a href="/feed" class="iconLink iconLinkFeed"><abbr title="Really Simple Syndication">RSS</abbr> feed</a></li>
            </ul>

            <?php print $footer_message . $footer ?>

        </div><!-- /foot -->

    </div><!-- /layoutBottom -->

    <?php print $closure ?>

</div><!-- /page -->

<script type="text/javascript" src="<?= $theme_path ?>/script-dependencies.min.js"></script>
<script type="text/javascript" src="<?= $theme_path ?>/script.js"></script>

</body>
</html>
